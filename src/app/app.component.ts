import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { StatesService } from './services/states.service';
import { State } from './services/state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private statesService: StatesService) { }

  title = 'Brazilian States';
  states: State[];
  filteredStates: State[];

  ngOnInit() {
    this.states = this.filteredStates = this.statesService.getStates();
  }

  search(searchWord): void {
    this.filteredStates = this.states.filter(state => state.name.toUpperCase().includes(searchWord.toUpperCase()));
  }

}
