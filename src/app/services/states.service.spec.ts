import { TestBed } from '@angular/core/testing';

import { StatesService } from './states.service';

describe('StatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatesService = TestBed.get(StatesService);
    expect(service).toBeTruthy();
  });

  it('should have 27 states including DF', () => {
    const service: StatesService = TestBed.get(StatesService);
    expect(service.getStates().length).toEqual(27);
  });
});
