import { Injectable } from '@angular/core';
import { State } from './state';

@Injectable({
  providedIn: 'root'
})
export class StatesService {

  private states: State[] = [
    { name: 'Acre', initial: 'AC', capital: 'Rio Branco', region: 'Norte', regionInitial: 'N' },
    { name: 'Alagoas', initial: 'AL', capital: 'Maceió', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Amapa', initial: 'AP', capital: 'Macapá', region: 'Norte', regionInitial: 'N' },
    { name: 'Amazonas', initial: 'AM', capital: 'Manaus', region: 'Norte', regionInitial: 'N' },
    { name: 'Bahia', initial: 'BA', capital: 'Salvador', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Ceara', initial: 'CE', capital: 'Fortaleza', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Distrito Federal', initial: 'DF', capital: 'Brasília', region: 'Centro-Oeste', regionInitial: 'CO' },
    { name: 'Espirito Santo', initial: 'ES', capital: 'Vitória', region: 'Sudeste', regionInitial: 'SE' },
    { name: 'Goias', initial: 'GO', capital: 'Goiânia', region: 'Centro-Oeste', regionInitial: 'CO' },
    { name: 'Maranhao', initial: 'MA', capital: 'São Luís', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Mato Grosso', initial: 'MT', capital: 'Cuiabá', region: 'Centro-Oeste', regionInitial: 'CO' },
    { name: 'Mato Grosso do Sul', initial: 'MS', capital: 'Campo Grande', region: 'Centro-Oeste', regionInitial: 'CO' },
    { name: 'Minas Gerais', initial: 'MG', capital: 'Belo Horizonte', region: 'Sudeste', regionInitial: 'SE' },
    { name: 'Para', initial: 'PA', capital: 'Belém', region: 'Norte', regionInitial: 'N' },
    { name: 'Paraiba', initial: 'PB', capital: 'João Pessoa', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Parana', initial: 'PR', capital: 'Curitiba', region: 'Sul', regionInitial: 'S' },
    { name: 'Pernambuco', initial: 'PE', capital: 'Recife', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Piaui', initial: 'PI', capital: 'Teresina', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Rio de Janeiro', initial: 'RJ', capital: 'Rio de Janeiro', region: 'Sudeste', regionInitial: 'SE' },
    { name: 'Rio Grande do Norte', initial: 'RN', capital: 'Natal', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Rio Grande do Sul', initial: 'RS', capital: 'Porto Alegre', region: 'Sul', regionInitial: 'S' },
    { name: 'Rondonia', initial: 'RO', capital: 'Porto Velho', region: 'Norte', regionInitial: 'N' },
    { name: 'Roraima', initial: 'RR', capital: 'Boa Vista', region: 'Norte', regionInitial: 'N' },
    { name: 'Santa Catarina', initial: 'SC', capital: 'Florianópolis', region: 'Sul', regionInitial: 'S' },
    { name: 'Sao Paulo', initial: 'SP', capital: 'São Paulo', region: 'Sudeste', regionInitial: 'SE' },
    { name: 'Sergipe', initial: 'SE', capital: 'Aracaju', region: 'Nordeste', regionInitial: 'NE' },
    { name: 'Tocantins', initial: 'TO', capital: 'Palmas', region: 'Norte', regionInitial: 'N' }
  ];

  constructor() { }

  getStates(): State[] {
    return this.states;
  }
}
