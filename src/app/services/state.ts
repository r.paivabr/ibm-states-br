export interface State {
    name: string;
    initial: string;
    capital: string; 
    region: string;
    regionInitial: string;
}
