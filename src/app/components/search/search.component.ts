import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  @Output() changes: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      search: new FormControl('')
    });

    // Another way of setInjectImplementation, filtering while user typing on search input
    //
    // this.searchForm.get('search').valueChanges.subscribe(searchWord => {
    //   this.filteredStates = this.states.filter(state => state.name.toUpperCase().includes(searchWord.toUpperCase()));
    // });
  }

  search() {
    const searchWord = this.searchForm.get('search').value;
    this.changes.emit(searchWord);
  }

}
