import { Component, OnInit, Input } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';
import { MatDialog } from '@angular/material';
import { State } from 'src/app/services/state';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() state: State;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openModal() {
    this.dialog.open(ModalComponent);
  }

}
